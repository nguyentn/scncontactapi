﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScnContact.Data.Entities
{
    [Table("ScnContactTranslation")]
    public class ContactTranslation
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Opinion { get; set; }
        [MaxLength(3)]
        public string Language { get; set; }
        [ForeignKey("ContactId")]
        public Guid ContactId { get; set; }
        public virtual Contact Contacts { get; set; }

        public DateTime? CreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
