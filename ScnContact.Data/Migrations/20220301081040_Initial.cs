﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ScnContact.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ScnContactInfo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Opinion = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    LastModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScnContactInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ScnContactTranslation",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Opinion = table.Column<string>(nullable: true),
                    Language = table.Column<string>(maxLength: 3, nullable: true),
                    ContactId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    LastModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScnContactTranslation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ScnContactTranslation_ScnContactInfo_ContactId",
                        column: x => x.ContactId,
                        principalTable: "ScnContactInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "ScnContactInfo",
                columns: new[] { "Id", "CreatedDate", "Email", "LastModifiedDate", "Name", "Opinion", "Phone" },
                values: new object[,]
                {
                    { new Guid("6a9adb7a-f26e-4419-ba07-8ceafaa9927d"), new DateTime(2020, 2, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "tnn220900@gmail.com", null, "Truong Nhat Nguyen", "Lien lac de lam viec", "0889227374" },
                    { new Guid("f02a7d5f-d0da-4180-be1c-3347b33fe092"), new DateTime(2019, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "txs123456@gmail.com", null, "Thai Xuan Son", "Thong tin lien lac ca nhan", "0365587692" },
                    { new Guid("2a8b6ba1-b21f-4c39-822a-e888d9ade861"), new DateTime(2019, 8, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "tvh241199@gmail.com", null, "Tran Van Hoang", "Thong tin cong khai", "0948765122" },
                    { new Guid("20b38722-82f4-4174-ada3-27a0c26eeec6"), new DateTime(2020, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "dmtit9999@gmail.com", null, "Do Minh Tuan", "Bi mat", "0773558524" },
                    { new Guid("843d930e-49fc-47e4-a545-b82834ee66c2"), new DateTime(2021, 1, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "nnl13579@gmail.com", null, "Nguyen Ngoc Long", "Thong tin lien lac rieng tu", "0943694738" }
                });

            migrationBuilder.InsertData(
                table: "ScnContactTranslation",
                columns: new[] { "Id", "ContactId", "CreatedDate", "Language", "LastModifiedDate", "Name", "Opinion" },
                values: new object[,]
                {
                    { new Guid("ca1f45b2-a361-426c-b217-be13f36b6e08"), new Guid("6a9adb7a-f26e-4419-ba07-8ceafaa9927d"), new DateTime(2021, 1, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "ENG", null, "Tieng Anh", "opinion_eng" },
                    { new Guid("c9584289-1a07-4512-a56f-2f92d2dc31d3"), new Guid("6a9adb7a-f26e-4419-ba07-8ceafaa9927d"), new DateTime(2021, 1, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "JP", null, "Tieng Nhat", "opinion_jp" },
                    { new Guid("d14cb846-5f16-433d-8e34-04490dbeb0be"), new Guid("f02a7d5f-d0da-4180-be1c-3347b33fe092"), new DateTime(2021, 1, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "KR", null, "Tieng Han", "opinion_kr" },
                    { new Guid("ff2cb8a1-5246-4616-8381-eb92e79d3e2d"), new Guid("2a8b6ba1-b21f-4c39-822a-e888d9ade861"), new DateTime(2021, 1, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "ENG", null, "Tieng Anh", "opinion_eng" },
                    { new Guid("0acedad2-f3d7-4f20-beb6-991e99ee5aba"), new Guid("20b38722-82f4-4174-ada3-27a0c26eeec6"), new DateTime(2021, 1, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "ESP", null, "Tieng Tay Ban Nha", "opinion_esp" },
                    { new Guid("c85661d0-71b1-4e9b-85da-06f1e40bef1a"), new Guid("20b38722-82f4-4174-ada3-27a0c26eeec6"), new DateTime(2021, 1, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "ENG", null, "Tieng Anh", "opinion_eng" },
                    { new Guid("73299343-2af0-42d6-b8c2-a40048d5a92c"), new Guid("843d930e-49fc-47e4-a545-b82834ee66c2"), new DateTime(2021, 1, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "CN", null, "Tieng Trung", "opinion_cn" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ScnContactTranslation_ContactId",
                table: "ScnContactTranslation",
                column: "ContactId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ScnContactTranslation");

            migrationBuilder.DropTable(
                name: "ScnContactInfo");
        }
    }
}
