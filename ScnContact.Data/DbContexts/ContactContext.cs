﻿using Microsoft.EntityFrameworkCore;
using ScnContact.Data.Entities;
using System;
using System.Collections.Generic;

namespace ScnContact.Data.DbContexts
{
    public class ContactContext : DbContext
    {
        public ContactContext(DbContextOptions<ContactContext> options) : base(options)
        {
        }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<ContactTranslation> ContactTranslations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>().HasData(
            new Contact
            {
                Id = Guid.Parse("6a9adb7a-f26e-4419-ba07-8ceafaa9927d"),
                Name = "Truong Nhat Nguyen",
                Email = "tnn220900@gmail.com",
                Phone = "0889227374",
                Opinion = "Lien lac de lam viec",
                CreatedDate = new DateTime(2020, 2, 19)
            },
            new Contact
            {
                Id = Guid.Parse("f02a7d5f-d0da-4180-be1c-3347b33fe092"),
                Name = "Thai Xuan Son",
                Email = "txs123456@gmail.com",
                Phone = "0365587692",
                Opinion = "Thong tin lien lac ca nhan",
                CreatedDate = new DateTime(2019, 5, 2)
            },
            new Contact
            {
                Id = Guid.Parse("2a8b6ba1-b21f-4c39-822a-e888d9ade861"),
                Name = "Tran Van Hoang",
                Email = "tvh241199@gmail.com",
                Phone = "0948765122",
                Opinion = "Thong tin cong khai",
                CreatedDate = new DateTime(2019, 8, 20)
            },
            new Contact
            {
                Id = Guid.Parse("20b38722-82f4-4174-ada3-27a0c26eeec6"),
                Name = "Do Minh Tuan",
                Email = "dmtit9999@gmail.com",
                Phone = "0773558524",
                Opinion = "Bi mat",
                CreatedDate = new DateTime(2020, 10, 1)
            },
            new Contact
            {
                Id = Guid.Parse("843d930e-49fc-47e4-a545-b82834ee66c2"),
                Name = "Nguyen Ngoc Long",
                Email = "nnl13579@gmail.com",
                Phone = "0943694738",
                Opinion = "Thong tin lien lac rieng tu",
                CreatedDate = new DateTime(2021, 1, 22)
            });

            modelBuilder.Entity<ContactTranslation>().HasData(
                new ContactTranslation
                {
                    Id = Guid.Parse("ca1f45b2-a361-426c-b217-be13f36b6e08"),
                    Name = "Tieng Anh",
                    Opinion = "opinion_eng",
                    Language = "ENG",
                    ContactId = Guid.Parse("6a9adb7a-f26e-4419-ba07-8ceafaa9927d"),
                    CreatedDate = new DateTime(2021, 1, 22)
                },
                new ContactTranslation
                {
                    Id = Guid.Parse("c9584289-1a07-4512-a56f-2f92d2dc31d3"),
                    Name = "Tieng Nhat",
                    Opinion = "opinion_jp",
                    Language = "JP",
                    ContactId = Guid.Parse("6a9adb7a-f26e-4419-ba07-8ceafaa9927d"),
                    CreatedDate = new DateTime(2021, 1, 22)
                },
                new ContactTranslation
                {
                    Id = Guid.Parse("d14cb846-5f16-433d-8e34-04490dbeb0be"),
                    Name = "Tieng Han",
                    Opinion = "opinion_kr",
                    Language = "KR",
                    ContactId = Guid.Parse("f02a7d5f-d0da-4180-be1c-3347b33fe092"),
                    CreatedDate = new DateTime(2021, 1, 22)
                },
                new ContactTranslation
                {
                    Id = Guid.Parse("ff2cb8a1-5246-4616-8381-eb92e79d3e2d"),
                    Name = "Tieng Anh",
                    Opinion = "opinion_eng",
                    Language = "ENG",
                    ContactId = Guid.Parse("2a8b6ba1-b21f-4c39-822a-e888d9ade861"),
                    CreatedDate = new DateTime(2021, 1, 22)
                },
                new ContactTranslation
                {
                    Id = Guid.Parse("0acedad2-f3d7-4f20-beb6-991e99ee5aba"),
                    Name = "Tieng Tay Ban Nha",
                    Opinion = "opinion_esp",
                    Language = "ESP",
                    ContactId = Guid.Parse("20b38722-82f4-4174-ada3-27a0c26eeec6"),
                    CreatedDate = new DateTime(2021, 1, 22)
                },
                new ContactTranslation
                {
                    Id = Guid.Parse("c85661d0-71b1-4e9b-85da-06f1e40bef1a"),
                    Name = "Tieng Anh",
                    Opinion = "opinion_eng",
                    Language = "ENG",
                    ContactId = Guid.Parse("20b38722-82f4-4174-ada3-27a0c26eeec6"),
                    CreatedDate = new DateTime(2021, 1, 22)
                },
                new ContactTranslation
                {
                    Id = Guid.Parse("73299343-2af0-42d6-b8c2-a40048d5a92c"),
                    Name = "Tieng Trung",
                    Opinion = "opinion_cn",
                    Language = "CN",
                    ContactId = Guid.Parse("843d930e-49fc-47e4-a545-b82834ee66c2"),
                    CreatedDate = new DateTime(2021, 1, 22)
                });

            base.OnModelCreating(modelBuilder);
        }
    }

}
