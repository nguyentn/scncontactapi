﻿using AutoMapper;
using ScnContact.Bussiness.Services;
using ScnContact.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScnContact.API.AutoMapper
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<Contact, ContactModel>()
                .ForMember(
                    dest => dest.CreatedDate,
                    opt => opt.MapFrom(src => $"{src.CreatedDate:dd-MM-yyyy}"))
                .ForMember(dest => dest.ContactTranslations, x => x.MapFrom(src => src.ContactTranslations));
            CreateMap<Contact, ContactCreateUpdateModel>();
            CreateMap<ContactTranslation, TranslationModel>();
        }
    }

}
