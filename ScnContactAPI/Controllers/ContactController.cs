﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ScnContact.Bussiness.Services;
using ScnContact.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScnContact.API.Controllers
{
    [ApiController]
    [Route("api/{groupName}/v1/contact")]
    [ApiExplorerSettings(GroupName = "Contacts")]
    public class ContactController : ControllerBase
    {
        private readonly IContactHandler _contactHandler;

        public ContactController(IContactHandler contactHandler)
        {
            _contactHandler = contactHandler;
        }

        /// <summary>
        /// Hiển thị tất cả bản ghi Contact
        /// </summary>
        /// <response code="200">Thành công</response>
        [HttpGet]
        [ProducesResponseType(typeof(ContactModel), StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<ContactModel>>> GetContacts([FromQuery] ContactParameters contactParameters)
        {
            var result = await _contactHandler.GetContacts(contactParameters);

            return Helper.TransformData(result);
        }

        /// <summary>
        /// Hiển thị một bản ghi Contact và danh sách Translation của nó
        /// </summary>
        /// <param name="id">id - Id bản ghi</param>
        /// <response code="200">Thành công</response>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetContact(Guid id)
        {
            var result = await _contactHandler.GetContact(id);

            return Helper.TransformData(result);
        }

        /// <summary>
        /// Thêm mới bản ghi Contact và danh sách Translation (nếu có)
        /// </summary>
        /// <param name="contactVm">Dữ liệu bản ghi</param>
        /// <response code="200">Thành công</response>
        [HttpPost]
        public async Task<IActionResult> CreateContact([FromBody] ContactModel contactVm)
        {
            if (contactVm == null)
                return new BadRequestResult();

            return Helper.TransformData(await _contactHandler.CreateContact(contactVm));
        }

        /// <summary>
        /// Cập nhật bản ghi Contact và danh sách Translation của nó
        /// </summary>
        /// <param name="id">id - Id bản ghi</param>
        /// <param name="contactVm">Dữ liệu bản ghi</param>
        /// <response code="200">Thành công</response>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateContact(Guid id, [FromBody] ContactCreateUpdateModel contactVm)
        {
            if (contactVm == null)
                return new BadRequestResult();

            var result = await _contactHandler.UpdateContact(id, contactVm);

            return Helper.TransformData(result);
        }


        /// <summary>
        /// Xóa bản ghi Contact và danh sách Translation của nó
        /// </summary>
        /// <param name="id">id - Id bản ghi</param>
        /// <response code="200">Thành công</response>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteContact(Guid id)
        {
            var result = await _contactHandler.DeleteContact(id);

            return Helper.TransformData(result);
        }
    }

}
