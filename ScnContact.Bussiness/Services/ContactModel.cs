﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ScnContact.Bussiness.Services
{
    public class ContactModel
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Opinion { get; set; }
        public string CreatedDate { get; set; }
        public List<TranslationModel> ContactTranslations { get; set; }
    }
    public class ContactCreateUpdateModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Opinion { get; set; }
        public List<TranslationModel> ContactTranslations { get; set; }
    }

    public class TranslationModel
    {
        public string Name { get; set; }
        public string Opinion { get; set; }
        [MaxLength(3)]
        public string Language { get; set; }
    }

    public class ContactParameters
    {
        public string search { get; set; }
        const int maxPageSize = 20;
        public int PageNumber { get; set; } = 1;
        private int _pageSize { get; set; } = 20;
        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > maxPageSize) ? maxPageSize : value;
        }
    }
}
