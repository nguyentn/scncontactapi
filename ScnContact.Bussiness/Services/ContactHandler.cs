﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ScnContact.Data.DbContexts;
using ScnContact.Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using System.Net;
using System.Linq.Expressions;
using LinqKit;
using ScnContact.Common.Utils;
using System;

namespace ScnContact.Bussiness.Services
{
    public class ContactHandler : IContactHandler
    {
        private readonly ContactContext _context;
        private readonly IMapper _mapper;

        public ContactHandler(ContactContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Response> CreateContact(ContactModel contactVm)
        {
            try
            {
                var contactToAdd = new Contact();
                contactToAdd.Id = Guid.NewGuid();
                contactToAdd.Name = contactVm.Name;
                contactToAdd.Email = contactVm.Email;
                contactToAdd.Phone = contactVm.Phone;
                contactToAdd.Opinion = contactVm.Opinion;
                contactToAdd.CreatedDate = DateTime.Now;

                //Tao translation neu co truyen vao
                if (contactVm.ContactTranslations.Count > 0)
                {
                    //Tao 1 danh sach Translation
                    contactToAdd.ContactTranslations = new List<ContactTranslation>();
                    //Tao tung` translation neu truyen vao nhieu hon 1 translation
                    foreach (var itemTranslation in contactVm.ContactTranslations)
                    {
                        var newTranslation = new ContactTranslation();
                        newTranslation.ContactId = contactToAdd.Id;
                        newTranslation.Id = Guid.NewGuid();
                        newTranslation.Name = itemTranslation.Name;
                        newTranslation.Opinion = itemTranslation.Opinion;
                        newTranslation.Language = itemTranslation.Language;
                        contactToAdd.ContactTranslations.Add(newTranslation);
                    }
                }
                _context.Add(contactToAdd);

                var status = await _context.SaveChangesAsync();
                var data = _mapper.Map<Contact, ContactCreateUpdateModel>(contactToAdd);

                return new ResponseObject<ContactCreateUpdateModel>(data);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                Log.Error("Param: {@Param}", contactVm);
                return new Response<ContactModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }

        public async Task<Response> DeleteContact(Guid id)
        {
            try
            {
                var iqueryable = await _context.Contacts.Include(d => d.ContactTranslations).Where(i => i.Id == id).FirstOrDefaultAsync();
                if (iqueryable == null)
                    return new ResponseError(HttpStatusCode.NotFound, "Contact not found");

                _context.Contacts.Remove(iqueryable);
                await _context.SaveChangesAsync();

                return new ResponseDelete(HttpStatusCode.OK, "Delete successfully", id, "");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(HttpStatusCode.InternalServerError, "Something went wrong: " + ex.Message);
            }
        }

        public async Task<Response> GetContact(Guid id)
        {
            try
            {
                var iqueryable = await _context.Contacts.Include(d => d.ContactTranslations).Where(i => i.Id == id)
                                                                                            .FirstOrDefaultAsync();
                if (iqueryable == null)
                    return new Response(HttpStatusCode.NotFound, "Contact not found");

                var result = _mapper.Map<Contact, ContactModel>(iqueryable);

                return new ResponseObject<ContactModel>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Param: {@Param}", id);
                return new ResponseError(HttpStatusCode.InternalServerError, "Something went wrong: " + ex.Message);
            }
        }

        public async Task<Response> GetContacts(ContactParameters contactParameters)
        {
            try
            {
                var predicate = BuildQuery(contactParameters);
                var queryResult = _context.Contacts.AsQueryable();
                var dataSet = queryResult.Include(d => d.ContactTranslations);

                var displayList = new List<Contact>();

                if (predicate == null)
                {
                    displayList = await dataSet.Skip(contactParameters.PageSize * (contactParameters.PageNumber - 1))
                                        .Take(contactParameters.PageSize).ToListAsync();
                }
                else
                {
                    displayList = await dataSet.Where(predicate)
                                        .Skip(contactParameters.PageSize * (contactParameters.PageNumber - 1))
                                        .Take(contactParameters.PageSize).ToListAsync();
                }

                var result = _mapper.Map<List<Contact>, List<ContactModel>>(displayList);
                if (displayList == null)
                    return new ResponseError(HttpStatusCode.NotFound, "Contacts not found");
                return new ResponseObject<List<ContactModel>>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(HttpStatusCode.InternalServerError, "Something went wrong: " + ex.Message);
            }
        }

        public async Task<Response> UpdateContact(Guid id, ContactCreateUpdateModel contactVm)
        {
            try
            {
                //Get contact
                var contactToUpdate = await _context.Contacts.Where(i => i.Id == id).FirstOrDefaultAsync();
                if (contactToUpdate == null)
                    return new ResponseError(HttpStatusCode.NotFound, "Contact not found");

                //Update contact
                contactToUpdate.Name = contactVm.Name;
                contactToUpdate.Email = contactVm.Email;
                contactToUpdate.Phone = contactVm.Phone;
                contactToUpdate.Opinion = contactVm.Opinion;
                contactToUpdate.LastModifiedDate = DateTime.Now;

                if (contactVm.ContactTranslations != null && contactVm.ContactTranslations.Any())
                {
                    foreach (var translation in contactVm.ContactTranslations)
                    {
                        bool isUpdateTranslation = true;

                        //Get translation
                        var contactTranslation = await _context.ContactTranslations
                                                            .Where(i => i.ContactId == id && i.Language == translation.Language)
                                                            .FirstOrDefaultAsync();
                        //Tao mot translation moi khi get khong co
                        if (contactTranslation == null)
                        {
                            contactTranslation = new ContactTranslation();
                            contactTranslation.Id = Guid.NewGuid();
                            contactTranslation.Name = translation.Name;
                            contactTranslation.Opinion = translation.Opinion;
                            contactTranslation.Language = translation.Language;
                            contactTranslation.CreatedDate = DateTime.Now;
                            isUpdateTranslation = false;
                        }

                        //Update translation
                        contactTranslation.ContactId = contactToUpdate.Id;
                        contactTranslation.Name = translation.Name;
                        contactTranslation.Opinion = translation.Opinion;
                        contactTranslation.Language = translation.Language;
                        contactTranslation.LastModifiedDate = DateTime.Now;


                        //Neu var bool = false => them moi translation
                        if (!isUpdateTranslation)
                            _context.ContactTranslations.Add(contactTranslation);
                    }
                }

                await _context.SaveChangesAsync();

                var data = _mapper.Map<Contact, ContactCreateUpdateModel>(contactToUpdate);

                return new ResponseObject<ContactCreateUpdateModel>(data);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Param: {@Param}, id: {@id}", contactVm, id);
                return new Response<ContactModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }

        private Expression<Func<Contact, bool>> BuildQuery(ContactParameters contactParameters)
        {
            var predicate = PredicateBuilder.New<Contact>(true);

            if (!string.IsNullOrEmpty(contactParameters.search))
            {
                predicate.And(s => s.Name.ToLower().Contains(contactParameters.search.ToLower())
                                || s.Email.ToLower().Contains(contactParameters.search.ToLower())
                                || s.Phone.ToLower().Contains(contactParameters.search.ToLower()));
            }
            return predicate;
        }
    }
}
