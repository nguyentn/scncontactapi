﻿using ScnContact.Common.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ScnContact.Bussiness.Services
{
    public interface IContactHandler
    {
        Task<Response> GetContacts(ContactParameters contactParameters);
        Task<Response> GetContact(Guid id);
        Task<Response> CreateContact(ContactModel contactVm);
        Task<Response> UpdateContact(Guid id, ContactCreateUpdateModel contactVm);
        Task<Response> DeleteContact(Guid id);
    }

}
